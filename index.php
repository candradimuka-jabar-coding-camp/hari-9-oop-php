<?php

require_once('ape.php');
require_once('frog.php');

$sheep= new Animal("shaun");
echo "Name : ". $sheep->name ."<br>";
echo "legs : ". $sheep->leg ."<br>";
echo "cold blooded : ". $sheep->cold_blooded ."<br>";
echo "<br>";

$kodok  = new Frog("buduk");
echo "Name : ". $kodok ->name ."<br>";
echo "legs : ". $kodok ->leg ."<br>";
echo "cold blooded : ". $kodok ->cold_blooded ."<br>";
echo "Jump : ". $kodok ->jump() ."<br>";
echo "<br>";

$sungokong = new Ape("kera sakti");
echo "Name : ". $sungokong->name ."<br>";
echo "legs : ". $sungokong->leg ."<br>";
echo "cold blooded : ". $sungokong->cold_blooded ."<br>";
echo "Yell : ". $sungokong->yell() ."<br>";

?>